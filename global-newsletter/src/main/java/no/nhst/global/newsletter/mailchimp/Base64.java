package no.nhst.global.newsletter.mailchimp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class Base64 {
    private static BASE64Encoder encoder = new BASE64Encoder();
    private static BASE64Decoder decoder = new BASE64Decoder();
    private static Log logger = LogFactory.getLog(Base64.class);

    public Base64() {
    }

    public static String encode(String text) {
        return text == null?null:encoder.encode(text.getBytes());
    }

    public static String decode(String text) {
        if(text == null) {
            return null;
        } else {
            try {
                return new String(decoder.decodeBuffer(text));
            } catch (IOException var2) {
                logger.error("decode(" + text + ") failed", var2);
                return null;
            }
        }
    }
}

