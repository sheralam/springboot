package no.nhst.global.newsletter.mailchimp;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class AlreadySubscribedException extends NewsletterException {
	private static final long serialVersionUID = 1L;

	public AlreadySubscribedException() {

	}

	public AlreadySubscribedException(String message) {
		super(message);
	}

	public AlreadySubscribedException(Throwable cause) {
		super(cause);
	}

	public AlreadySubscribedException(String message, Throwable cause) {
		super(message, cause);
	}

}
