package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 12/9/16.
 */
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryStatus {
    private boolean enabled ;
    private boolean can_cancel;
    private String status;
    private Integer emails_sent;
    private Integer emails_canceled;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isCan_cancel() {
        return can_cancel;
    }

    public void setCan_cancel(boolean can_cancel) {
        this.can_cancel = can_cancel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getEmails_sent() {
        return emails_sent;
    }

    public void setEmails_sent(Integer emails_sent) {
        this.emails_sent = emails_sent;
    }

    public Integer getEmails_canceled() {
        return emails_canceled;
    }

    public void setEmails_canceled(Integer emails_canceled) {
        this.emails_canceled = emails_canceled;
    }
}
