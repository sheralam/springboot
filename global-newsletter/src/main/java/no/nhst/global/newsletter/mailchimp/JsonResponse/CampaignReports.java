package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 12/9/16.
 */
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignReports {
    private String id;
    private String campaign_title;
    private String type;
    private String list_id;
    private String subject_line;
    private Integer emails_sent;
    private String send_time;
    private Integer unsubscribed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampaign_title() {
        return campaign_title;
    }

    public void setCampaign_title(String campaign_title) {
        this.campaign_title = campaign_title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public String getSubject_line() {
        return subject_line;
    }

    public void setSubject_line(String subject_line) {
        this.subject_line = subject_line;
    }

    public Integer getEmails_sent() {
        return emails_sent;
    }

    public void setEmails_sent(Integer emails_sent) {
        this.emails_sent = emails_sent;
    }

    public String getSend_time() {
        return send_time;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }

    public Integer getUnsubscribed() {
        return unsubscribed;
    }

    public void setUnsubscribed(Integer unsubscribed) {
        this.unsubscribed = unsubscribed;
    }
}
