package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 9/7/16.
 */
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultMailchimpStatusResponse {
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
