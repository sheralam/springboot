package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 9/7/16.
 */
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class History {
    private String list_id;
    private String month;
    private int existing;
    private int imports;
    private int optins;

    public int getOptins() {
        return optins;
    }

    public void setOptins(int optins) {
        this.optins = optins;
    }


    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getExisting() {
        return existing;
    }

    public void setExisting(int existing) {
        this.existing = existing;
    }

    public int getImports() {
        return imports;
    }

    public void setImports(int imports) {
        this.imports = imports;
    }
}
