package no.nhst.global.newsletter.mailchimp;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class MailchimpException extends NewsletterException {
	private static final long serialVersionUID = 1L;

	private int errorCode;
	
	public MailchimpException(String message, int code) {
		super(message);
		this.errorCode = code;
	}

	public int getErrorCode( ) {
		return errorCode;
	}
	
	public String toString( ) {
		return super.toString() + " [" + errorCode + "]";
	}
}
