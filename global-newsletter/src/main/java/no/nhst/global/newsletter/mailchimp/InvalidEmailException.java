package no.nhst.global.newsletter.mailchimp;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class InvalidEmailException extends NewsletterException {
	private static final long serialVersionUID = 1L;

	public InvalidEmailException() {

	}

	public InvalidEmailException(String message) {
		super(message);
	}

	public InvalidEmailException(Throwable cause) {
		super(cause);
	}

	public InvalidEmailException(String message, Throwable cause) {
		super(message, cause);
	}

}
