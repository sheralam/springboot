package no.nhst.global.newsletter.mailchimp;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */

/**
 * this class supportds httpMethods for mailchimps since v3.0
 */

public enum HttpMethods {
    GET("GET"),
    POST("POST"),
    PATCH("PATCH"),
    PUT("PUT"),
    DELETE("DELETE");

    private final String Name;
    HttpMethods(String value) {
        this.Name = value;
    }

    public String getName() {
        return Name;
    }
}
