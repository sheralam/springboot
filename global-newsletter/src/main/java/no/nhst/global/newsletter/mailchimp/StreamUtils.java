package no.nhst.global.newsletter.mailchimp;

/**
 * Created by sheralam on 7/5/17.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class StreamUtils {
    private static final Log logger = LogFactory.getLog(StreamUtils.class);

    public StreamUtils() {
    }

    public static String convertToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException var13) {
            logger.error("Got exception reading stream", var13);
        } finally {
            try {
                is.close();
            } catch (IOException var12) {
                logger.error("Got exception closing stream", var12);
            }

        }

        return sb.toString();
    }
}

