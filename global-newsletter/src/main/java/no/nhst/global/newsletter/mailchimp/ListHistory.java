package no.nhst.global.newsletter.mailchimp;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListHistory {
	
	private String month;
	private int existing;
	private int imports;
	private int optins;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getExisting() {
		return existing;
	}
	public void setExisting(int existing) {
		this.existing = existing;
	}
	public int getImports() {
		return imports;
	}
	public void setImports(int imports) {
		this.imports = imports;
	}
	public int getOptins() {
		return optins;
	}
	public void setOptins(int optins) {
		this.optins = optins;
	}
	
}
