package no.nhst.global.newsletter.mailchimp;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */

/**
 * This class represent the functional parameters for mailchimp rest api urls
 */
public class MailchimpFunction {
    String Function;
    String FunctionValue;

    /**
     *
     * @param function , null is acceptable
     * @param functionValue , null is acceptable
     */
    public MailchimpFunction(String function, String functionValue) {
        this.Function = function;
        this.FunctionValue = functionValue;
    }

    public MailchimpFunction() {

    }

    public void setFunction(String function) {
        this.Function = function;
    }

    public void setFunctionValue(String functionValue) {
        this.FunctionValue = functionValue;
    }

    public String getFunction() {
        return Function;
    }

    public String getFunctionValue() {
        return FunctionValue;
    }

    @Override
    public String toString() {
        return (this.Function == null)? "" : "/" + this.Function + ((this.FunctionValue == null)?"":"/" + this.FunctionValue);
    }
}
