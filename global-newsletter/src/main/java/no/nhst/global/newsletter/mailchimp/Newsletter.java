package no.nhst.global.newsletter.mailchimp;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class Newsletter {
  private SimpleDateFormat dateFormat;
  private String subject;
  private String fromEmail;
  private String fromLine;
  private String toLine;
  private String content;
  private String contentUrl;
  private String analytics;
  private String actionType;
  private long inactiveDuration;
  private String scheduled;
  private String trackOpens;
  private String trackHtml_clicks;


  public void setDateFormat(SimpleDateFormat dateFormat) {
    this.dateFormat = dateFormat;
  }

  public SimpleDateFormat getDateFormat() {
    return dateFormat;
  }

  public void setSubject(String subject) {
    this.subject = subject.indexOf("@now") < 0 ? subject : subject.replace("@now", this.dateFormat.format(new Date()));
  }

  public String getSubject() {
    return subject;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  public void setContentUrl(String contentUrl) {
    // replacing production prefix from the content url to test
    String prefix = System.getProperty("nhst.server.prefix");
    if (prefix.contains("test")) {
      contentUrl = contentUrl.replace("www", "test");
    }
    this.contentUrl = contentUrl;
  }

  public String getContentUrl() {
    return contentUrl;
  }

  public void setFromEmail(String senderEmail) {
    this.fromEmail = senderEmail;
  }

  public String getFromEmail() {
    return fromEmail;
  }

  public void setFromLine(String senderLine) {
    this.fromLine = senderLine;
  }

  public String getFromLine() {
    return fromLine;
  }

  public void setToLine(String toLine) {
    this.toLine = toLine;
  }

  public String getToLine() {
    return toLine;
  }

  public String getAnalytics() {
    return analytics;
  }

  public void setAnalytics(String analytics) {
    this.analytics = analytics;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  public long getInactiveDuration() {
    return inactiveDuration;
  }

  public void setInactiveDuration(long inactiveDuration) {
    this.inactiveDuration = inactiveDuration;
  }
  
  public String getScheduled() {
	return scheduled;
  }

  public void setScheduled(String scheduled) {
	this.scheduled = scheduled;
  }

  public String getTrackOpens() {
	return trackOpens;
  }
	
  public void setTrackOpens(String trackOpens) {
	this.trackOpens = trackOpens;
  }

  public String getTrackHtml_clicks() {
	return trackHtml_clicks;
  }
	
  public void setTrackHtml_clicks(String trackHtml_clicks) {
	this.trackHtml_clicks = trackHtml_clicks;
  }

}
