package no.nhst.global.newsletter.mailchimp;

import no.nhst.global.newsletter.mailchimp.Base64;
import no.nhst.global.newsletter.mailchimp.StreamUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
/**
 * This is Utility class for fetching content from Mailchimp with different http method
 */
public class HttpMethodsUtils {

    // JSON Mapper
    private static final ObjectMapper mapper = new ObjectMapper();


    public static void printErr(String errStr){
        System.err.println(errStr);
    }

    public static String fetchContent(String urlString) throws MalformedURLException, IOException {
        URL url = createEncodedUrl(urlString);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.connect();

        return StreamUtils.convertToString(connection.getInputStream());
    }

    /**
     *
     * @param urlString
     * @param responseType in which type of response caller will expect
     * @return the same responseType
     * @throws MailchimpException
     * @throws NewsletterException
     */
    public static <T> T fetchContent(String urlString, Class<T> responseType) throws MailchimpException, NewsletterException {
        String jsonText;
        printErr("-------------------------------------------------------------\n");
        printErr("--------------------- fetchContent for " +responseType.getSimpleName());
        printErr("--------------------- fetchContent for " +urlString);

        try {
            jsonText = fetchContent(urlString);
        } catch (MalformedURLException e) {
            throw new NewsletterException("Error in Mailchimp URL generation", e);
        } catch (IOException e) {
            throw new NewsletterException("Error talking to Mailchimp server", e);
        }
        printErr("fetchContent 1:\n\n" + jsonText);
        printErr("-------------------------------------------------------------\n\n\n\n");
        return parseJson(jsonText, responseType);
    }

    /**
     *
     * @param urlString
     * @param responseType
     * @param method HttpMethods enum expected
     * @return content as responseType
     * @throws NewsletterException
     */
    private static <T> T fetchContent(String urlString, Class<T> responseType,HttpMethods method) throws  NewsletterException {

        return fetchContent(urlString,responseType,method,null);

    }

    /**
     *
     * @param urlString
     * @param method HttpMethods enum expected
     * @param parameters
     * @param responseType
     * @return content as string
     * @throws MailchimpException
     * @throws NewsletterException
     */
    public static <T> T fetchContent(String urlString, Class<T> responseType, HttpMethods method, Map<String, String> parameters) throws MailchimpException, NewsletterException {


        try {
            // if this is a GET request.
            if(method.getName().equalsIgnoreCase(HttpMethods.GET.getName())){
                throw  new NewsletterException("GET is not supported right now !!");
            }


            //printErr("--------------------- fetchContent for Method : " +method.getName());
            //printErr("--------------------- fetchContent for Caller : " + parameters==null?"No params ":parameters.get("caller") + " #response:" + responseType == null ? "responseType==null" : responseType.getSimpleName());
            URL url = createEncodedUrl(urlString);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod(method.getName());
            connection.setRequestProperty("Authorization", "Basic " +Base64.encode("apikey:"+parameters.get("apikey")));
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            if(parameters.get("data")!=null){
                try {
                    JSONObject jsonObj = new JSONObject(parameters.get("data"));
                    writer.write(jsonObj.toString());
                } catch (JSONException e) {
                    //printErr("--------------------- fetchContent for eRROR Invalid JSON : " + parameters.get("data"));
                    e.printStackTrace();
                }

            }
            writer.flush();

            if(responseType == null){
                //printErr("--------------------- fetchContent responseType : NULL");
                connection.getInputStream();
                return null;
            }
            String str = StreamUtils.convertToString(connection.getInputStream());
            //printErr("\n\n\n###########  fetchContent(...) "+method.getName()+" method Response:: \n\n");
            //printErr(str);
            //printErr("\n\n\n ################################### fetchContent(...) "+method.getName()+" method END Response");
            return parseJson(str, responseType);
        } catch (MalformedURLException e) {
            //printErr("\n\n\n###########  fetchContent(...) "+method.getName()+" method  MalformedURLException    ERRORRRR "+e.getLocalizedMessage());
            throw new NewsletterException("Error in Mailchimp URL generation", e);
        } catch (IOException e) {
            //printErr("\n\n\n###########  fetchContent(...) "+method.getName()+" method   IOException  ERRORRRR "+e.getLocalizedMessage());
            throw new NewsletterException("Error talking to Mailchimp server", e);
        }


    }

    /**
     *
     * @param parameters
     * @return string with all the params added as string
     * @throws UnsupportedEncodingException
     */
    private static String getParameters(Map<String, String> parameters) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        boolean first = true;
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            String pName = entry.getKey();
            String pValue = entry.getValue();

            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            result.append(pName);
            result.append("=");
            result.append(pValue);
        }

        return result.toString();
    }


    public static <T> T parseJson(String jsonText, Class<T> expectedType) throws MailchimpException, NewsletterException {
        T result;

        try {
            result = mapper.readValue(jsonText, expectedType);
        } catch (JsonParseException e) {
            throw new NewsletterException("Reply from MailChimp was not valid JSON :  " + jsonText, e);
        } catch (JsonMappingException e) {
            if (expectedType.isAssignableFrom(Map.class)) {
                throw new NewsletterException("Reply from MailChimp could not be parsed as a JSON map :  " + jsonText, e);
            } else {
                // It might be a Mailchimp error message
                throw parseAsMailchimpError(jsonText);
            }
        } catch (IOException e) {
            throw new NewsletterException("Program logic error. IOException should not be possible here.", e);
        }

        return result;
    }


    private static MailchimpException parseAsMailchimpError(String jsonText) throws NewsletterException {
        @SuppressWarnings("unchecked")
        Map<String, Object> errorMap = (Map<String, Object>) parseJson(jsonText, Map.class);

        Object error = errorMap.get("error");
        Object code = errorMap.get("code");

        if (error instanceof String && code instanceof Integer) {
            return new MailchimpException((String) error, (Integer) code);
        } else {
            throw new NewsletterException("Not a valid Mailchimp error :  " + jsonText);
        }
    }
    private static URL createEncodedUrl(String encodable) throws MalformedURLException {
        String encodedUrl = encodable.replaceAll("\\s+", "+");
        return new URL(encodedUrl);
    }
}
