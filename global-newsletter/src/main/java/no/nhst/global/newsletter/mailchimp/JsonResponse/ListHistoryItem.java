package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 9/7/16.
 * This is the adapter for ListHistory class
 */
import no.nhst.global.newsletter.mailchimp.ListHistory;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListHistoryItem {
    private List<History> history;

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public ListHistory[] getListHistory() {
        ListHistory[] listHistories = new ListHistory[getHistory().size()];
        ListHistory listHistory;
        for(int  i=0; i<history.size();i++){
            listHistory = new ListHistory();
            listHistory.setExisting(history.get(i).getExisting());
            listHistory.setImports(history.get(i).getExisting());
            listHistory.setMonth(history.get(i).getMonth());
            listHistory.setOptins(history.get(i).getOptins());
            listHistories[i] = listHistory;
        }
        return listHistories;
    }


}
