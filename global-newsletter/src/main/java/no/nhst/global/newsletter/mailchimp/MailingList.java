package no.nhst.global.newsletter.mailchimp;

import java.util.List;
import java.util.Map;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class MailingList {	
	// The human-readable name of the list
	private String listName;
	// A human-readable description of the list
	private String listDescription;
	// A unique ID for this mailing list
	private String listId;
	// Interest groups, mapped by category (So, f.i. { 'Region' => {'Europe', 'Asia', 'America'}, 'CargoType' => { 'General', 'Tanker', 'Dry-Bulk', 'Multipurpos' } }
	private Map<String, List<String>> interestGroup;
	// Set to true if this mailing list should only be available to subscribing customers
	private boolean subscribersOnly = false;
	
	public void setListName(String listName) {
		this.listName = listName;
	}

	public String getListName() {
		return listName;
	}

	public void setListDescription(String listDescription) {
		this.listDescription = listDescription;
	}

	public String getListDescription() {
		return listDescription;
	}

	public void setListId(String listId) {
		this.listId = listId;
	}

	public String getListId() {
		return listId;
	}

	public void setInterestGroup(Map<String, List<String>> interestGroup) {
		this.interestGroup = interestGroup;
	}

	public Map<String, List<String>> getInterestGroup() {
		return interestGroup;
	}

	public void setSubscribersOnly(boolean subscribersOnly) {
		this.subscribersOnly = subscribersOnly;
	}

	public boolean isSubscribersOnly() {
		return subscribersOnly;
	}

    @Override
    public String toString() {
        return "MailingList{" +
                "listName='" + listName + '\'' +
                ", listDescription='" + listDescription + '\'' +
                ", listId='" + listId + '\'' +
                ", interestGroup=" + interestGroup +
                ", subscribersOnly=" + subscribersOnly +
                '}';
    }
}
