package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 9/9/16.
 */
public class MailchimpBodyRequestDataCreator {
    /**
     *
     * @param email
     * @param type eg. subscribed/unsubscribed/cleaned/pending
     * @return
     */
    public static String createSubscribeUserPostData(String email,String type){
        String data = "{";
        data = data + "\"status_if_new\": \""+type+"\",";
        data = data + "\"email_address\": \""+email +"\"";
        data = data + "}";
        return data;
    }
}
