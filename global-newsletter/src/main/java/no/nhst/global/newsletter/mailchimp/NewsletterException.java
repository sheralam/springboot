package no.nhst.global.newsletter.mailchimp;

/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */
public class NewsletterException extends Exception {
	private static final long serialVersionUID = 1L;

	public NewsletterException() {

	}

	public NewsletterException(String message) {
		super(message);

	}

	public NewsletterException(Throwable cause) {
		super(cause);
	}

	public NewsletterException(String message, Throwable cause) {
		super(message, cause);
	}

}
