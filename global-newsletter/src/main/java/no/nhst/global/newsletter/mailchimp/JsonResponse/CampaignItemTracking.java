package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 9/7/16.
 */
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignItemTracking {
    private String html_clicks;
    private String opens;
    private String goal_tracking;
    private String google_analytics;

    public String getHtml_clicks() {
        return html_clicks;
    }

    public void setHtml_clicks(String html_clicks) {
        this.html_clicks = html_clicks;
    }

    public String getOpens() {
        return opens;
    }

    public void setOpens(String opens) {
        this.opens = opens;
    }

    public String getGoal_tracking() {
        return goal_tracking;
    }

    public void setGoal_tracking(String goal_tracking) {
        this.goal_tracking = goal_tracking;
    }

    public String getGoogle_analytics() {
        return google_analytics;
    }

    public void setGoogle_analytics(String google_analytics) {
        this.google_analytics = google_analytics;
    }
}
