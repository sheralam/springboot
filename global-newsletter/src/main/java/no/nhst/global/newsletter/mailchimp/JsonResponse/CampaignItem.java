package no.nhst.global.newsletter.mailchimp.JsonResponse;

/**
 * Created by sheralam on 9/7/16.
 */
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignItem {

    private String id;
    private String type;
    private String status;
    private String create_time;
    private String send_time;
    private CampaignItemSettings settings;
    private CampaignItemTracking tracking;
    private DeliveryStatus delivery_status;

    public DeliveryStatus getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(DeliveryStatus delivery_status) {
        this.delivery_status = delivery_status;
    }

    public CampaignItemTracking getTracking() {
        return tracking;
    }

    public void setTracking(CampaignItemTracking tracking) {
        this.tracking = tracking;
    }

    public CampaignItemSettings getSettings() {
        return settings;
    }

    public void setSettings(CampaignItemSettings settings) {
        this.settings = settings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getSend_time() {
        return send_time;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }
}
