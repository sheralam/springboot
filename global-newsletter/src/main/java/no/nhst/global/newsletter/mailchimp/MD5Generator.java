package no.nhst.global.newsletter.mailchimp;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */

/**
  * This Class generate MD5 hash code for an given Email
 */
public class MD5Generator {

        public final static String generateMD5Hash(String emailAddres){
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(emailAddres.getBytes());
                byte byteData[] = md.digest();

                //convert the byte to hex format method 2
                StringBuffer hexString = new StringBuffer();
                for (int i = 0; i < byteData.length; i++) {
                    String hex = Integer.toHexString(0xff & byteData[i]);
                    if (hex.length() == 1) hexString.append('0');
                    hexString.append(hex);
                }

                return hexString.toString();
            }catch (NoSuchAlgorithmException nsae){
                return "";
            }
        }

}
