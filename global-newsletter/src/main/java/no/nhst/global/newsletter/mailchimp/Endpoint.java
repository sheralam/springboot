package no.nhst.global.newsletter.mailchimp;


/**
 *
 * July, 2017
 * @author Sher E Alam (sher.alam@nhst.no)
 */

/**
 * This enum class represent the top most categories of mailchimp api functional endpoints
 */
public enum Endpoint {
    ROOT ("/"),
    AUTHORISEZ_APP("/authorized-apps"),
    AUTOMATION("/automations"),
    BATCH_OPERATIONS("/batches"),
    CAMPAIGN_FOLDER("/campaign-folders"),
    CAMPAIGNS("/campaigns"),
    CONVERSATIONS("/conversations"),
    E_COMMERCE("/ecommerce/stores"),
    FILE_MANAGER_FILES("/file-manager/files"),
    FILE_MANAGER_FOLDERS("/file-manager/folders"),
    LISTS("/lists"),
    REPORTS("/reports"),
    TEMPLATE_FOLDER("/template-folders"),
    TEMPLATES("/templates/");

    private final String mEndpoint;
    Endpoint(String pEndpoint){
        this.mEndpoint = pEndpoint;
    }

    public String getmEndpoint() {
        return this.mEndpoint;
    }
}
